mindspore.Tensor.neg
====================

.. py:method:: mindspore.Tensor.neg()

    计算 `self` 的相反数并返回。

    .. math::
        out_{i} = - tensor_{i}

    返回：
        Tensor，shape和数据类型与 `self` 相同。
