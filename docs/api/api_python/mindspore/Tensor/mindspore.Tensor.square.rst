mindspore.Tensor.square
=======================

.. py:method:: mindspore.Tensor.square()

    逐元素计算 `self` 的平方。

    .. math::
        out_{i} = self_{i} ^ 2

    返回：
        Tensor，数据类型和shape与 `self` 相同。
