mindspore.Tensor.gt
====================

.. py:method:: mindspore.Tensor.gt(other)

    详情请参考 :func:`mindspore.ops.gt`。
