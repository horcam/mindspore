mindspore.Tensor.div\_
======================

.. py:method:: mindspore.Tensor.div_(other, *, rounding_mode=None)

    :func:`mindspore.Tensor.div` 的in-place版本。