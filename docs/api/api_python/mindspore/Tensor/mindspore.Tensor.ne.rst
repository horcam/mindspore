mindspore.Tensor.ne
===================

.. py:method:: mindspore.Tensor.ne(other)

    :func:`mindspore.Tensor.not_equal` 的别名。