mindspore.Tensor.abs
====================

.. py:method:: mindspore.Tensor.abs()

    逐元素计算 `self` 的绝对值。

    .. math::
        out_i = |self_i|

    返回：
        Tensor，shape与 `self` 相同。
