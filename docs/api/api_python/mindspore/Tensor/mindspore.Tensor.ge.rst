mindspore.Tensor.ge
===================

.. py:method:: mindspore.Tensor.ge(other)

    :func:`mindspore.Tensor.greater_equal` 的别名。
