mindspore.Tensor.reciprocal
============================

.. py:method:: mindspore.Tensor.reciprocal()

    返回输入Tensor每个元素的倒数。

    .. math::
        out_{i} =  \frac{1}{self_{i}}

    返回：
        Tensor，shape与 `self` 相同。

