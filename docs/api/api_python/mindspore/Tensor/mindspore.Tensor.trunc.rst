mindspore.Tensor.trunc
======================

.. py:method:: mindspore.Tensor.trunc()

    返回一个新的Tensor，该Tensor具有输入元素的截断整数值。

    返回：
        Tensor， shape和数据类型与 `self` 相同。
