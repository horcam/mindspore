mindspore.Tensor.logical_not
============================

.. py:method:: mindspore.Tensor.logical_not()

    逐元素计算一个Tensor的逻辑非运算。

    .. math::
        out_{i} = \neg self_{i}

    输出：
        Tensor，shape与 `self` 相同，数据类型为bool。
