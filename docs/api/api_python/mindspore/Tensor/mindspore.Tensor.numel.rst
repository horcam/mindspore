mindspore.Tensor.numel
=======================

.. py:method:: mindspore.Tensor.numel()

    返回一个表示张量元素个数的整型数值。

    返回：
        标量，表示张量元素个数的整数类型。