mindspore.hal.communication_stream
==================================

.. py:function:: mindspore.hal.communication_stream()

    返回此设备上的通信流。

    .. note::
        - 接口即将废弃，请使用接口 :func:`mindspore.runtime.communication_stream` 代替。

    返回：
        Stream，此设备上的通信流。
