mindspore.hal.current_stream
============================

.. py:function:: mindspore.hal.current_stream()

    返回此设备上正在使用的流。

    .. note::
        - 接口即将废弃，请使用接口 :func:`mindspore.runtime.current_stream` 代替。

    返回：
        Stream，此设备上正在使用的流。
