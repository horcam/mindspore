mindspore.hal.default_stream
============================

.. py:function:: mindspore.hal.default_stream()

    返回此设备上的默认流。

    .. note::
        - 接口即将废弃，请使用接口 :func:`mindspore.runtime.default_stream` 代替。

    返回：
        Stream，此设备上的默认流。
