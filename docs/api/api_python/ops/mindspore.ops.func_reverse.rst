mindspore.ops.reverse
==============================

.. py:function:: mindspore.ops.reverse(x, axis)

    此接口将在未来版本弃用，请使用 :func:`mindspore.ops.flip` 代替。
