sinh:
    description: |
        Computes hyperbolic sine of the input element-wise.

        .. math::

            output_i = \sinh(input_i)

        Args:
            input (Tensor): The input tensor of hyperbolic sine function.
                Supported dtypes are as follows: 

                - GPU/CPU: float16, float32, float64, complex64 or complex128.
                - Ascend: bool, int8, uint8, int16, int32, int64, float16, float32, float64, complex64, complex128 or bfloat16.

        Returns:
            Tensor, has the same shape as the `input`. 
            The dtype of output is float32 when dtype of `input` is in
            [bool, int8, uint8, int16, int32, int64]. Otherwise output has the same dtype as the `input`.

        :raise TypeError: If `input` is not a Tensor.
        :raise TypeError:
            * CPU/GPU: If dtype of `input` is not float16, float32 or float64, complex64, complex128.
            * Ascend: If dtype of `input` is not bool, int8, uint8, int16, int32, int64, float16, float32, float64, complex64, complex128 or bfloat16.

        Supported Platforms:
            ``Ascend`` ``GPU`` ``CPU``

        Examples:
            >>> import mindspore
            >>> import numpy as np
            >>> from mindspore import Tensor, ops
            >>> input = Tensor(np.array([0.62, 0.28, 0.43, 0.62]), mindspore.float32)
            >>> output = ops.sinh(input)
            >>> print(output)
            [0.6604918  0.28367308 0.44337422 0.6604918 ]
