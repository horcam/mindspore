unsqueeze:
  description: |
    unsqueeze(dim) -> Tensor
    
    Adds an additional dimension to `self` at the given dim.

    Args:
        dim (int): Specifies the dimension index at which to expand
            the shape of `self`. The value of `dim` must be in the range
            `[-self.ndim-1, self.ndim]`. Only constant value is allowed.

    Returns:
        Tensor, the shape of tensor is :math:`(1, n_1, n_2, ..., n_R)` if the
        value of `dim` is 0. It has the same data type as `self`.

    Raises:
        TypeError: If `dim` is not an int.
        ValueError: If `dim` is not in the valid range :math:`[-self.ndim-1, self.ndim]`.

    Supported Platforms:
        ``Ascend`` ``GPU`` ``CPU``

    Examples:
        >>> import mindspore
        >>> import numpy as np
        >>> from mindspore import Tensor
        >>> input_tensor = Tensor(np.array([[2, 2], [2, 2]]), mindspore.float32)
        >>> output = input_tensor.unsqueeze(dim=0)
        >>> print(output)
        [[[2. 2.]
          [2. 2.]]]
