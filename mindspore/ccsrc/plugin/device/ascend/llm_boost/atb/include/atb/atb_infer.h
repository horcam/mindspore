/*
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * AscendTransformerBoost is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef ATB_ATBINFER_H
#define ATB_ATBINFER_H
#include "atb/context.h"
#include "atb/graph_op_builder.h"
#include "atb/operation.h"
#include "atb/svector.h"
#include "atb/types.h"
#include "atb/utils.h"
#include "atb/infer_op_params.h"
#include "atb/train_op_params.h"
#include "atb/context.h"
#endif

//!
//! \mainpage
//!
//! This is the API documentation for the AscendTransformerBoost. It provides information on individual
//! functions, classes and methods. Use the index on the left to navigate the documentation.
//!