/**
 * Copyright 2024 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "plugin/device/ascend/kernel/ge/ge_kernel_mod.h"
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <functional>
#include "ir/tensor.h"
#include "abstract/ops/primitive_infer_map.h"
#include "pybind_api/gil_scoped_long_running.h"
#include "ops_utils/op_utils.h"
#include "backend/ge_backend/graph_ir/utils.h"

namespace mindspore {
namespace kernel {
namespace {
const char kAlreadyInitMemory[] = "kAlreadyInitMemory";
}

bool GeKernelMod::Init(const std::vector<KernelTensor *> &inputs, const std::vector<KernelTensor *> &outputs) {
  MS_LOG(DEBUG) << "GeKernelMod Init";
  return true;
}

bool GeKernelMod::Launch(const std::vector<KernelTensor *> &inputs, const std::vector<KernelTensor *> &workspace,
                         const std::vector<KernelTensor *> &outputs, void *stream_ptr) {
  MS_EXCEPTION_IF_NULL(stream_ptr);

  if (skip_run_) {
    return true;
  }

  MS_EXCEPTION_IF_NULL(graph_executor_);
  MS_EXCEPTION_IF_NULL(graph_);

  if (!workspace.empty()) {
    graph_executor_->SetGraphWorkspaceMemory(graph_, workspace[0]->device_ptr(), workspace[0]->size());
  }

  auto ret = graph_executor_->RunGraphRefModeForKernel(graph_, node_, inputs, outputs, stream_ptr);
  if (!ret) {
    MS_LOG(EXCEPTION) << "Launch ge graph failed, graph id: " << std::to_string(graph_->graph_id());
  }

  return true;
}

void GeKernelMod::InitGeMemory(size_t stream_id) const {
  MS_EXCEPTION_IF_NULL(graph_executor_);
  MS_EXCEPTION_IF_NULL(graph_);
  if (!graph_->has_flag(kAlreadyInitMemory)) {
    graph_executor_->InitGEFixMemory(graph_, stream_id);
    graph_->set_flag(kAlreadyInitMemory, true);
  }
}

bool GeKernelMod::IsNeedUpdateOutputShapeAndSize() {
  MS_EXCEPTION_IF_NULL(graph_);
  if (graph_->is_dynamic_shape()) {
    return true;
  }
  return false;
}
}  // namespace kernel
}  // namespace mindspore
