/**
 * Copyright 2025 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "plugin/device/ascend/optimizer/ir_fusion_infer/rms_norm_quant_fusion.h"

#include <vector>
#include <string>
#include <utility>

#include "utils/ms_context.h"
#include "mindspore/ops/op_def/nn_ops.h"
#include "include/common/utils/anfalgo.h"
#include "ir/primitive.h"
#include "include/backend/anf_runtime_algorithm.h"
#include "include/backend/optimizer/helper.h"
#include "mindspore/ops/op_def/auto_generate/gen_ops_primitive_a.h"
#include "mindspore/ops/op_def/auto_generate/gen_ops_primitive_q.h"
#include "mindspore/ops/op_def/auto_generate/gen_ops_primitive_r.h"
#include "mindspore/ops/op_def/auto_generate/gen_ops_primitive_s.h"
#include "mindspore/ops/op_def/auto_generate/gen_ops_primitive_t.h"

namespace mindspore {
namespace opt {
std::vector<std::string> RmsNormQuantFusion::MustExistPrimitiveName() const {
  std::vector<std::string> ret{prim::kPrimRmsNorm->name(), prim::kPrimAdd->name(), prim::kPrimQuantV2->name()};
  return ret;
}

inline bool IsZero(const BaseRef &n) {
  if (utils::isa<AnfNodePtr>(n)) {
    auto value_ptr = utils::cast<ValueNodePtr>(n);

    if (value_ptr == nullptr) {
      return false;
    }

    auto idx_value = GetValue<int64_t>(value_ptr->value());
    if (idx_value == 0) {
      return true;
    }
  }

  return false;
}

const BaseRef RmsNormQuantFusion::DefinePattern() const {
  auto index0 = std::make_shared<CondVar>(IsConstant);
  auto rms_norm = VectorRef({prim::kPrimRmsNorm, x1_, gamma_, eps_});

  auto tuple_get_item_0 = VectorRef({prim::kPrimTupleGetItem, rms_norm, index0});
  auto add = VectorRef({prim::kPrimAdd, tuple_get_item_0, beta0_});

  auto sqrt_mode0 = std::make_shared<CondVar>(IsConstant);      // not used
  auto rounding_mode0 = std::make_shared<CondVar>(IsConstant);  // not used
  auto dst_type0 = std::make_shared<CondVar>(IsConstant);       // not used
  auto quant = VectorRef({prim::kPrimQuantV2, add, scale0_, offset0_, sqrt_mode0, rounding_mode0, dst_type0});
  return quant;
}

static bool IsSupport(const FuncGraphPtr &graph, const AnfNodePtr &node, const AnfNodePtr &rms_norm) {
  auto x_dtype = common::AnfAlgo::GetPrevNodeOutputInferDataType(rms_norm, 0);
  auto gamma_dtype = common::AnfAlgo::GetPrevNodeOutputInferDataType(rms_norm, 1);
  auto scale_dtype = common::AnfAlgo::GetPrevNodeOutputInferDataType(node, 1);
  auto offset_dtype = common::AnfAlgo::GetPrevNodeOutputInferDataType(node, 2);

  if (x_dtype != kNumberTypeFloat16 && x_dtype != kNumberTypeBFloat16) {
    MS_LOG(INFO) << "RmsNormQuant fused failed because of unsupported x_dtype: " << x_dtype;
    return false;
  }

  if (x_dtype != gamma_dtype || x_dtype != scale_dtype) {
    MS_LOG(INFO) << "RmsNormQuant fused failed because of  inconsistent dtype, x_dtype: " << x_dtype
                 << ", gamma_dtype: " << gamma_dtype << ", scale_dtype: " << scale_dtype;
    return false;
  }

  if (offset_dtype != kNumberTypeInt8) {
    MS_LOG(INFO) << "RmsNormQuant fused failed because of  unsupported offset_dtype: " << offset_dtype;
    return false;
  }

  // if rstd is used, do not fuse
  auto rms_norm_users = GetRealNodeUsedList(graph, rms_norm);
  for (const auto &user : *rms_norm_users) {
    const auto &get_item_node = user.first;
    if (!IsPrimitiveCNode(get_item_node, prim::kPrimTupleGetItem)) {
      continue;
    }

    auto get_item_index = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(get_item_node), 1);
    auto get_item_index_value_ptr = get_item_index->cast<ValueNodePtr>();
    if (get_item_index_value_ptr == nullptr) {
      MS_LOG(INFO) << "RmsNormQuant fused failed because the index in TupleGetItem is not constant: "
                   << get_item_index->DebugString() << ", get_item_node: " << get_item_node->DebugString();
      return false;
    }

    constexpr auto kRstdIndexInRmsNormOut = 1;
    auto index_value = GetValue<int64_t>(get_item_index_value_ptr->value());
    if (index_value == kRstdIndexInRmsNormOut) {
      auto rstd_users = GetRealNodeUsedList(graph, get_item_index);
      if (rstd_users->size() != 0) {
        MS_LOG(INFO) << "RmsNormQuant fused failed because rstd is used by some node";
        return false;
      }

      break;
    }
  }

  return true;
}

static constexpr auto kRmsNormOut2OneAddQuant = 1;
static constexpr auto kRmsNormOut2TwoAddQuant = 2;
static constexpr auto kRmsNormOut2OneAddQuantAndOneShape = 3;
static constexpr auto kRmsNormOut2TwoAddQuantAndOneShape = 4;

// the num of Add is more than kAddNumTwo, or one of the users is not add-quant
static constexpr auto kUnsupportedTag = 0xffff;

void GetAddAndShapeNum(const FuncGraphPtr &graph,
                       const std::shared_ptr<std::vector<std::pair<AnfNodePtr, int>>> &rms_norm_out0_users,
                       size_t *add_num, size_t *shape_num, AnfNodePtr *shape_node) {
  for (const auto &user : *rms_norm_out0_users) {
    const auto &user_node = user.first;
    if (IsPrimitiveCNode(user_node, prim::kPrimAdd)) {
      auto add_users = GetRealNodeUsedList(graph, user_node);
      if (add_users->size() != 1) {
        MS_LOG(INFO) << "RmsNormQuant fuse failed because the user of Add is more than one: " << add_users->size();
        return;
      }

      if (!IsPrimitiveCNode(add_users->at(0).first, prim::kPrimQuantV2)) {
        MS_LOG(INFO) << "RmsNormQuant fuse failed because the user of Add is not Quant: "
                     << add_users->at(0).first->fullname_with_scope();
        return;
      }
      ++(*add_num);
    } else if (IsPrimitiveCNode(user_node, prim::kPrimShape)) {
      ++(*shape_num);
      *shape_node = user_node;
    }
  }
}

inline size_t GetOpsCaseAfterRmsNorm(const FuncGraphPtr &graph, const AnfNodePtr &rms_norm_out0,
                                     AnfNodePtr *shape_node) {
  auto users = GetRealNodeUsedList(graph, rms_norm_out0);

  auto user_num = users->size();
  size_t add_num = 0;
  size_t shape_num = 0;

  GetAddAndShapeNum(graph, users, &add_num, &shape_num, shape_node);

  if (user_num == 1) {
    if (add_num != 1) {
      MS_LOG(INFO) << "RmsNormQuant fuse failed because the user of RmsNorm is not Add-Quant";
      return kUnsupportedTag;
    }

    return kRmsNormOut2OneAddQuant;
  }

  if (user_num == 2) {
    if (shape_num == 1 && add_num == 1) {
      return kRmsNormOut2OneAddQuantAndOneShape;
    }

    if (add_num == user_num) {
      return kRmsNormOut2TwoAddQuant;
    }

    MS_LOG(INFO)
      << "RmsNormQuant fuse failed because the num of Add and shape in users of RmsNorm is invalid, add_num: "
      << add_num << ", shape_num: " << shape_num;
    return kUnsupportedTag;
  }

  if (user_num == 3) {
    if (shape_num == 1 && add_num == 2) {
      return kRmsNormOut2TwoAddQuantAndOneShape;
    }

    MS_LOG(INFO)
      << "RmsNormQuant fuse failed because the num of Add and shape in users of RmsNorm is invalid, add_num: "
      << add_num << ", shape_num: " << shape_num;
    return kUnsupportedTag;
  }

  return kUnsupportedTag;
}

static const AnfNodePtr CreateRmsNormQuantNode(const FuncGraphPtr &graph, const AnfNodePtr &node, const AnfNodePtr &x1,
                                               const AnfNodePtr &gamma, const AnfNodePtr &beta, const AnfNodePtr &scale,
                                               const AnfNodePtr &offset, const AnfNodePtr &eps) {
  auto prim = std::make_shared<Primitive>("RmsNormQuant");
  std::vector<AnfNodePtr> inputs = {NewValueNode(prim), x1, gamma, beta, scale, offset, eps};
  auto rms_norm_quant = graph->NewCNode(inputs);
  MS_EXCEPTION_IF_NULL(rms_norm_quant);

  std::vector<TypeId> types;
  std::vector<BaseShapePtr> shapes;
  auto output_num = AnfAlgo::GetOutputElementNum(node);
  for (size_t i = 0; i < output_num; i++) {
    types.push_back(common::AnfAlgo::GetOutputInferDataType(node, i));
    shapes.push_back(AnfAlgo::GetOutputDetailShape(node, i));
  }

  common::AnfAlgo::SetOutputTypeAndDetailShape(types, shapes, rms_norm_quant.get());
  rms_norm_quant->set_scope(node->scope());

  auto build_info = GenerateKernelBuildInfo(rms_norm_quant);
  AnfAlgo::SetSelectKernelBuildInfo(build_info, rms_norm_quant.get());

  return rms_norm_quant;
}

const AnfNodePtr RmsNormQuantFusion::RmsNormQuantFuseWithOnePath(const FuncGraphPtr &graph, const AnfNodePtr &node,
                                                                 const EquivPtr &equiv,
                                                                 const AnfNodePtr &shape_node) const {
  auto x1 = utils::cast<AnfNodePtr>((*equiv)[x1_]);
  auto gamma = utils::cast<AnfNodePtr>((*equiv)[gamma_]);
  auto beta = utils::cast<AnfNodePtr>((*equiv)[beta0_]);
  auto scale = utils::cast<AnfNodePtr>((*equiv)[scale0_]);
  auto offset = utils::cast<AnfNodePtr>((*equiv)[offset0_]);
  auto eps = utils::cast<AnfNodePtr>((*equiv)[eps_]);

  AnfNodePtr shape_input_node = nullptr;
  if (shape_node != nullptr) {
    shape_input_node = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(shape_node), 0);
    if (shape_input_node == nullptr) {
      MS_LOG(INFO) << "RmsNormQuant fused failed because shape_input_node is nullptr";
      return nullptr;
    }
  }

  auto rms_norm_quant = CreateRmsNormQuantNode(graph, node, x1, gamma, beta, scale, offset, eps);

  if (shape_node != nullptr) {
    auto mng = graph->manager();
    MS_EXCEPTION_IF_NULL(mng);
    (void)mng->Replace(shape_input_node, rms_norm_quant);
  }

  return rms_norm_quant;
}

#define IsByteAlign(size, len) ((size & (len - 1)) == 0)

template <typename T>
inline bool DataNotEqual(void *data_c0, void *data_c1, size_t size) {
  auto ptr0 = reinterpret_cast<T *>(data_c0);
  auto ptr1 = reinterpret_cast<T *>(data_c1);

  auto elem = size / sizeof(T);
  for (size_t i = 0; i < elem; ++i) {
    if (ptr0[i] != ptr1[i]) {
      return true;
    }
  }

  return false;
}

inline bool ValueNotEqual(void *data_c0, void *data_c1, size_t size) {
  if (IsByteAlign(size, 8)) {
    return DataNotEqual<int64_t>(data_c0, data_c1, size);
  }

  if (IsByteAlign(size, 4)) {
    return DataNotEqual<int32_t>(data_c0, data_c1, size);
  }

  if (IsByteAlign(size, 2)) {
    return DataNotEqual<int16_t>(data_c0, data_c1, size);
  }

  if (IsByteAlign(size, 1)) {
    return DataNotEqual<int8_t>(data_c0, data_c1, size);
  }

  return true;
}

inline bool ParameterNotEqual(const std::string &name, const AnfNodePtr &load0, const AnfNodePtr &load1) {
  if (load0 == load1) {
    return false;
  }

  auto load_input0 = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(load0), 0);
  auto param_ptr0 = load_input0->cast<ParameterPtr>();
  auto load_input1 = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(load1), 0);
  auto param_ptr1 = load_input1->cast<ParameterPtr>();

  if (param_ptr0 == nullptr || param_ptr1 == nullptr) {
    MS_LOG(INFO) << "One of the parameter is nullptr for " << name << ", param_ptr0: " << param_ptr0
                 << ", param_ptr1: " << param_ptr1;
    return true;
  }

  if (!param_ptr0->has_default() || !param_ptr1->has_default()) {
    MS_LOG(INFO) << "One of the parameter does not have default value for " << name
                 << ", param_ptr0: " << param_ptr0->has_default() << ", param_ptr1: " << param_ptr1->has_default();
    return true;
  }

  auto value_ptr0 = param_ptr0->default_param();
  auto value_ptr1 = param_ptr1->default_param();

  if (value_ptr0 == nullptr || value_ptr1 == nullptr) {
    MS_LOG(INFO) << "One of the value is nullptr for " << name << ", value_ptr0: " << value_ptr0
                 << ", value_ptr1: " << value_ptr1;
    return true;
  }

  auto tensor_ptr0 = value_ptr0->cast<tensor::TensorPtr>();
  auto tensor_ptr1 = value_ptr1->cast<tensor::TensorPtr>();

  if (tensor_ptr0 == nullptr || tensor_ptr1 == nullptr) {
    MS_LOG(INFO) << "One of the Tensor is nullptr for " << name << ", tensor_ptr0: " << tensor_ptr0
                 << ", tensor_ptr1: " << tensor_ptr1;
    return true;
  }

  auto data_c0 = tensor_ptr0->data_c();
  auto data_c1 = tensor_ptr1->data_c();

  if (data_c0 == nullptr || data_c1 == nullptr) {
    MS_LOG(INFO) << "One of the data_c is nullptr for " << name << ", data_c0: " << data_c0 << ", data_c1: " << data_c1;
    return true;
  }

  auto size0 = tensor_ptr0->Size();
  auto size1 = tensor_ptr0->Size();
  if (size0 != size1) {
    MS_LOG(INFO) << "The size is not equal, size0: " << size0 << ", size1: " << size1;
    return true;
  }

  return ValueNotEqual(data_c0, data_c1, size0);
}

const AnfNodePtr RmsNormQuantFusion::RmsNormQuantFuseWithTwoPath(const FuncGraphPtr &graph, const AnfNodePtr &node,
                                                                 const EquivPtr &equiv, const AnfNodePtr &rms_norm_out0,
                                                                 const AnfNodePtr &shape_node) const {
  auto x1 = utils::cast<AnfNodePtr>((*equiv)[x1_]);
  auto gamma = utils::cast<AnfNodePtr>((*equiv)[gamma_]);
  auto beta0_load = utils::cast<AnfNodePtr>((*equiv)[beta0_]);
  auto scale0 = utils::cast<AnfNodePtr>((*equiv)[scale0_]);
  auto offset0 = utils::cast<AnfNodePtr>((*equiv)[offset0_]);
  auto eps = utils::cast<AnfNodePtr>((*equiv)[eps_]);

  AnfNodePtr shape_input_node = nullptr;
  if (shape_node != nullptr) {
    shape_input_node = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(shape_node), 0);
    if (shape_input_node == nullptr) {
      MS_LOG(INFO) << "RmsNormQuant fused failed because shape_input_node is nullptr";
      return nullptr;
    }
  }

  AnfNodePtr beta1_load = nullptr;
  auto rms_norm_out0_users = GetRealNodeUsedList(graph, rms_norm_out0);

  AnfNodePtr second_path_add_node = nullptr;
  AnfNodePtr add0_node = nullptr;
  auto beta0_load_users = GetRealNodeUsedList(graph, beta0_load);
  for (const auto &user : *beta0_load_users) {
    if (IsPrimitiveCNode(utils::cast<CNodePtr>(user.first), prim::kPrimAdd)) {
      add0_node = user.first;
      break;
    }
  }

  for (const auto &user : *rms_norm_out0_users) {
    if (user.first == shape_node) {
      continue;
    }

    const auto add_node = user.first;
    auto load = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(add_node), 1);
    if (!IsPrimitiveCNode(load, prim::kPrimLoad)) {
      MS_LOG(INFO) << "RmsNormQuant fuse failed because the input node is not load when add-quant number is 2, input: "
                   << load->DebugString();
      return nullptr;
    }

    if (add0_node == add_node) {
      continue;
    }

    beta1_load = load;
    second_path_add_node = add_node;
    break;
  }

  if (ParameterNotEqual("beta", beta0_load, beta1_load)) {
    MS_LOG(INFO) << "RmsNormQuant fuse failed because the value of beta is not equal.";
    return nullptr;
  }

  if (second_path_add_node == nullptr) {
    return nullptr;
  }

  auto add1_users = GetRealNodeUsedList(graph, second_path_add_node);
  const auto &quant_node1 = add1_users->at(0).first;
  static constexpr auto kScaleIdx = 1;
  static constexpr auto kOffsetIdx = 2;
  auto scale1 = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(quant_node1), kScaleIdx);
  auto offset1 = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(quant_node1), kOffsetIdx);

  if (ParameterNotEqual("scale", scale0, scale1)) {
    MS_LOG(INFO) << "RmsNormQuant fuse failed because the value of scale is not equal.";
    return nullptr;
  }

  if (ParameterNotEqual("offset", offset0, offset1)) {
    MS_LOG(INFO) << "RmsNormQuant fuse failed because the value of offset is not equal.";
    return nullptr;
  }

  auto rms_norm_quant_node = CreateRmsNormQuantNode(graph, node, x1, gamma, beta0_load, scale0, offset0, eps);

  auto mng = graph->manager();
  MS_EXCEPTION_IF_NULL(mng);
  (void)mng->Replace(quant_node1, rms_norm_quant_node);

  if (shape_node != nullptr) {
    (void)mng->Replace(shape_input_node, rms_norm_quant_node);
  }
  return rms_norm_quant_node;
}

const AnfNodePtr RmsNormQuantFusion::Process(const FuncGraphPtr &graph, const AnfNodePtr &node,
                                             const EquivPtr &equiv) const {
  auto ms_context = MsContext::GetInstance();
  MS_EXCEPTION_IF_NULL(ms_context);
  if (!ms_context->IsEnableInferBoost()) {
    MS_LOG(INFO) << "Internal op is disabled.";
    return nullptr;
  }

  const std::string fusion_op_name = "RmsNormQuant";
  auto enable_op_list = ms_context->ms_internal_enable_custom_kernel_list();
  bool enable_add_rmsnorm =
    (std::find(enable_op_list.begin(), enable_op_list.end(), fusion_op_name) != enable_op_list.end());
  if (!enable_add_rmsnorm) {
    MS_LOG(INFO) << "Internal RmsNormQuant is disabled.";
    return nullptr;
  }

  auto add_node = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(node), 0);
  auto rms_norm_out0 = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(add_node), 0);
  auto rms_norm_node = common::AnfAlgo::GetInputNode(utils::cast<CNodePtr>(rms_norm_out0), 0);
  MS_EXCEPTION_IF_NULL(rms_norm_node);

  if (!IsSupport(graph, node, rms_norm_node)) {
    MS_LOG(INFO) << "Can't fused to RmsNormQuant because of unsupported case.";
    return nullptr;
  }

  AnfNodePtr shape_node = nullptr;
  AnfNodePtr out_node = nullptr;
  auto num_of_add_after_rmsnorm = GetOpsCaseAfterRmsNorm(graph, rms_norm_out0, &shape_node);
  if (num_of_add_after_rmsnorm == kRmsNormOut2OneAddQuant ||
      num_of_add_after_rmsnorm == kRmsNormOut2OneAddQuantAndOneShape) {
    out_node = RmsNormQuantFuseWithOnePath(graph, node, equiv, shape_node);
  } else if (num_of_add_after_rmsnorm == kRmsNormOut2TwoAddQuant ||
             num_of_add_after_rmsnorm == kRmsNormOut2TwoAddQuantAndOneShape) {
    out_node = RmsNormQuantFuseWithTwoPath(graph, node, equiv, rms_norm_out0, shape_node);
  }

  if (out_node != nullptr) {
    MS_LOG(INFO) << "RmsNormQuant fused successfully with RmsNorm out case: " << num_of_add_after_rmsnorm;
  }

  return out_node;
}

}  // namespace opt
}  // namespace mindspore
